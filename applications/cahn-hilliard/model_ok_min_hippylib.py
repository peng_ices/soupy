from __future__ import absolute_import, division, print_function

import dolfin as dl
import numpy as np
path = "../../"
import sys
sys.path.append(path)
from soupy import *


# Class for poisson equation solve
class poisson_solve():
    def __init__(self, Uh, Zh):
        """Class for solving the possion equation with homogeneous Neumann boundary. The rhs is automatically projected in to the zero average space and the solution is also in the zero average space.
            """
        self.Uh = Uh
        self.Zh = Zh
        
        u_trial = dl.TrialFunction(Uh)
        u_test = dl.TestFunction(Uh)
        z_trial = dl.TrialFunction(Zh)
        (w_trial, lam_trial) = dl.split(z_trial)
        z_test = dl.TestFunction(Zh)
        (w_test, lam_test) = dl.split(z_test)
        a_varf = dl.inner(dl.grad(w_trial), dl.grad(w_test))*dl.dx + w_trial*lam_test*dl.dx + lam_trial*w_test*dl.dx
        self.A = dl.assemble(a_varf)
        
        self.M = dl.assemble(u_trial*u_test*dl.dx)
        
        self.solver = dl.PETScLUSolver()
        self.solver.set_operator(self.A)
    
    def init_vector(self, v, dim):
        """Initialized vector corresponing to space Uh"""
        self.M.init_vector(v,dim)
    
    def vector_U2Z(self, v, y):
        
        y.zero()
        w_temp = dl.Function(self.Uh)
        w_temp.vector()[:] = v
        z_test = dl.TestFunction(self.Zh)
        (w_test, lam_test) = dl.split(z_test)
        y.axpy(1., dl.assemble(w_temp*w_test*dl.dx))
    
    def vector_Z2U(self, v, y):
        
        y.zero()
        z_temp = dl.Function(self.Zh)
        z_temp.vector().zero()
        z_temp.vector().axpy(1., v)
        y.axpy(1., z_temp.split(deepcopy=True)[0].vector())
    
    def solve(self, v, y):
        
        rhs, sol = dl.Vector(), dl.Vector()
        self.A.init_vector(rhs, 1)
        self.A.init_vector(sol, 1)
        self.vector_U2Z(y, rhs)
        self.solver.solve(sol, rhs)
        self.vector_Z2U(sol, v)


class ok_min:
    def __init__(self, Vh, m, theta, save=True, print_level=3):
        """ Input: mesh, the state/parameter/adjoint function space, the finite element used to generate state (FE), the mass avegrae (m), Prior object, and print level (0 for no print, 1 for termination criteria only, 2 for additional step information, 3 for storing iterations as .vtu files).
        """
        mesh = Vh.mesh()
        P1 = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
        Vh_STATE = dl.FunctionSpace(mesh, P1)
        Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=3)
        Vh = [Vh_STATE, Vh_PARAMETER, Vh_STATE]
        self.Vh = Vh
        FE = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
        self.theta = dl.Function(Vh_PARAMETER)
        self.theta.vector()[:] = theta  # kappa/4 due to different double well potential

        #Initialize iteration count
        self.it = 0
        
        # Assign variables
        self.m      = m
        self.save = save
        self.print_level = print_level
        
        # Initialize parameter objects
        (self.kappa, self.eps, self.sigma) = dl.split(self.theta)
        
        # Calculate volume
        self.vol = dl.assemble(dl.Constant(1.0)*dl.dx(mesh))
        
        # Define Finite Elements for real value
        R = dl.FiniteElement("R", mesh.ufl_cell(), 0)
        
        #Define the function space for the mixed problem and poisson problem
        ME1 = dl.MixedElement([FE,FE])
        ME2 = dl.MixedElement([FE,R])
        self.Z1 = dl.FunctionSpace(mesh, ME1)
        self.Z2 = dl.FunctionSpace(mesh, ME2)
        
        # Define the space of the state
        self.U = Vh[STATE]
        
        # Define the test/trial functions in the mixed space
        z_test = dl.TestFunction(self.Z1)
        (u_test, mu_test) = dl.split(z_test)
        z_trial = dl.TrialFunction(self.Z1)
        (u_trial, mu_trial) = dl.split(z_trial)
        
        # Define the test/trial functions
        v_test = dl.TestFunction(self.U)
        v_trial = dl.TrialFunction(self.U)
        
        # Define the mass matrix in state
        self.M = dl.assemble(v_trial*v_test*dl.dx)
        
        # Initialize forward solver/poisson solver
        self.psolver = poisson_solve(self.U, self.Z2)
        
        # Define functions
        self.z  = dl.Function(self.Z1)
        (self.u, mu) = dl.split(self.z)
        self.w  = dl.Function(self.U)
        self.mu_true = dl.Function(self.U)
        
        # Double well potential and its derivative
        f = self.kappa*(dl.Constant(1.0)-self.u**2)**2
        dfdu = dl.Constant(4)*self.kappa*(self.u**2-dl.Constant(1.0))*self.u
        df2du2 = dl.Constant(8)*self.kappa*self.u**2 + dl.Constant(4)*self.kappa*(self.u**2-dl.Constant(1.0))
        df2du2_GN = dl.Constant(8)*self.kappa*self.u**2
        df2du2_scaled = dl.Constant(8)*self.kappa*self.u**2 + dl.Constant(0.5*4)*self.kappa*(self.u**2-dl.Constant(1.0))
        
        # Define energy variational form
        self.F_varf = f*dl.dx + dl.Constant(0.5)*self.eps**2*dl.inner(dl.grad(self.u),dl.grad(self.u))*dl.dx + dl.Constant(0.5)*self.sigma*dl.inner(dl.grad(self.w),dl.grad(self.w))*dl.dx
        
        # Define rhs for solving mu
        self.mu_rhs_varf = dfdu*v_test*dl.dx + self.eps**2*dl.inner(dl.grad(self.u),dl.grad(v_test))*dl.dx
        
        # Define Gradient weak form in mixed space
        self.grad_varf = dl.inner(dl.grad(self.mu_true), dl.grad(u_test))*dl.dx + self.sigma*(self.u-dl.Constant(self.m))*u_test*dl.dx
        
        # Define Gradient weak form in state space
        self.grad_u_varf = dl.inner(dl.grad(self.mu_true), dl.grad(v_test))*dl.dx + self.sigma*(self.u-dl.Constant(self.m))*v_test*dl.dx
        
        # Define Energy Gradient
        self.grad_F_varf = dfdu*v_test*dl.dx + self.eps**2*dl.inner(dl.grad(self.u), dl.grad(v_test))*dl.dx + self.sigma*self.w*v_test*dl.dx
        
        # Define Hessian weak form components
        H_uu = self.sigma*u_trial*u_test*dl.dx
        
        H_umu = dl.inner(dl.grad(mu_trial), dl.grad(u_test))*dl.dx
        
        H_muu = -df2du2*u_trial*mu_test*dl.dx - self.eps**2*dl.inner(dl.grad(u_trial), dl.grad(mu_test))*dl.dx
        
        H_GN_muu = -df2du2_GN*u_trial*mu_test*dl.dx - self.eps**2*dl.inner(dl.grad(u_trial), dl.grad(mu_test))*dl.dx
        
        H_scaled_muu = -df2du2_scaled*u_trial*mu_test*dl.dx - self.eps**2*dl.inner(dl.grad(u_trial), dl.grad(mu_test))*dl.dx
        
        H_mumu = mu_trial*mu_test*dl.dx
        
        # Define Hessian weak form
        self.H_varf = H_uu + H_umu + H_muu + H_mumu
        self.H_GN_varf = H_uu + H_umu + H_GN_muu + H_mumu
        self.H_scaled_varf = H_uu + H_umu + H_scaled_muu + H_mumu
        
        # Define Hessian matrix for mu_hat
        self.mu_hat_varf = df2du2*v_trial*v_test*dl.dx + self.eps**2*dl.inner(dl.grad(v_trial), dl.grad(v_test))*dl.dx

    def solveFwd(self, state, x, z0, tol=1e-12):
        
        # Assign model parameter
        self.theta.assign(x[PARAMETER])
        
        # Assign initial condition for z
        # z_init = InitialConditions(degree=1)
        self.z.assign(z0)
        
        # Solve the poisson problem for w
        self.psolver.solve(self.w.vector(), dl.project(self.u, self.U).vector())
        
        # Solve for mu
        mu_rhs = dl.assemble(self.mu_rhs_varf)
        dl.solve(self.M, self.mu_true.vector(), mu_rhs)
        
        # Assemble gradients
        gn = dl.assemble(self.grad_varf)
        gn_u = dl.assemble(self.grad_u_varf)
        gn_F = dl.assemble(self.grad_F_varf)
        
        # Evaluete energy
        Fn = dl.assemble(self.F_varf)
        
        # Initialize the update vectors/functions (dz,du,dw)
        dz = dl.Function(self.Z1)
        du, dw= dl.Vector(), dl.Vector()
        self.M.init_vector(du, 1)
        self.M.init_vector(dw, 1)
        
        # Define storage functions
        z_prev = dl.Function(self.Z1)
        w_prev = dl.Function(self.U)
        
        # Define the gradient represntation vector
        g = dl.Vector()
        self.M.init_vector(g, 1)
        
        # Solve for L2 Norm for gradient
        dl.solve(self.M, g, gn_u)
        gn_u_norm = np.sqrt(g.inner(gn_u))
        gn_u_norm_init = gn_u_norm

        # Solve for curvature in gradient direction
        mu_hat = dl.assemble(self.mu_hat_varf)
        H_grad = g.inner(mu_hat*g) + self.theta.vector()[2]*gn_F.inner(g)

        rel_gn_u_norm = gn_u_norm / gn_u_norm_init
        if self.print_level > 1:
            print("iter #", self.it + 1)
            print(" Residule relative L^2 norm: ", rel_gn_u_norm)
            print(" Residule of mass average: ", dl.assemble((self.z.sub(0) - dl.Constant(self.m)) * dl.dx))
            print(" Energy: ", Fn)

        # Newton iteration parameters
        self.it = 0
        max_it = 500
        max_backtrack = 50
        c_armijo = 1.e-4
        
        # Storage of solution
        if self.save:
            file = dl.File("data/u-reduced.pvd", "compressed")
            file << (self.z.split()[0], 0.)
    
        # Starting Newton iteration
        for self.it in range(max_it):
            
            # Backtracking double well potential
            for gn_iter in range(3):
                
                if gn_iter == 2 or self.it < 5:
                    Hn = dl.assemble(self.H_GN_varf)
                    if self.print_level > 1:
                        print("Switch to Gauss-Newton Hessian...")
                elif gn_iter == 1:
                    Hn = dl.assemble(self.H_scaled_varf)
                    if self.print_level > 1:
                        print("Switch to scaled Hessian...")
                else:
                    Hn = dl.assemble(self.H_varf)
                
                dl.solve(Hn, dz.vector(), -gn)
                du.zero()
                du.axpy(1., dz.split(deepcopy = True)[0].vector())
                grad_du = gn_F.inner(du)
                if grad_du <= 0.0 or self.it < 5:
                    break
        
            #Backtrack
            z_prev.assign(self.z)
            w_prev.assign(self.w)
            alpha = 1.
            bk_converged = False
            self.psolver.solve(dw, du)
            if self.it > 0:
                for j in range(max_backtrack):
                    self.z.assign(z_prev)
                    self.w.assign(w_prev)
                    self.z.vector().axpy(alpha, dz.vector())
                    self.w.vector().axpy(alpha, dw)
                    Fnext = dl.assemble(self.F_varf)
                    if Fnext < Fn + alpha*c_armijo*grad_du:
                        Fn = Fnext
                        bk_converged = True
                        if self.print_level > 1:
                            print("Backtracking converges with alpha = ", alpha)
                        break
                    alpha *= 0.5
            if not bk_converged:
                alpha = 1.0
                self.z.assign(z_prev)
                self.w.assign(w_prev)
                self.z.vector().axpy(1, dz.vector())
                if self.it > 0:
                    self.w.vector().axpy(1., dw)
                else:
                    self.psolver.solve(self.w.vector(), dl.project(self.u, self.U).vector())
                    Fn = dl.assemble(self.F_varf)
                if self.print_level > 1:
                    print("Backtracking does not converge.")
                    
            # Solve for mu
            mu_rhs = dl.assemble(self.mu_rhs_varf)
            dl.solve(self.M, self.mu_true.vector(), mu_rhs)
            
            # Assemble gradients
            gn = dl.assemble(self.grad_varf)
            gn_u = dl.assemble(self.grad_u_varf)
            gn_F = dl.assemble(self.grad_F_varf)
            
            # Solve for L2 Norm for gradient
            dl.solve(self.M, g, gn_u)
            gn_u_norm = np.sqrt(g.inner(gn_u))

            # Solve for curvature in gradient direction
            mu_hat = dl.assemble(self.mu_hat_varf)
            H_grad = g.inner(mu_hat*g) + self.theta.vector()[2]*gn_F.inner(g)

            rel_gn_u_norm = gn_u_norm / gn_u_norm_init
            if self.save:
                file << (self.z.split()[0], self.it+1.)
            if self.print_level > 1:
                print("iter #", self.it+1)
                print(" Residule relative L^2 norm: ", rel_gn_u_norm)
                print(" Residule of mass average: ", dl.assemble((self.z.sub(0)-dl.Constant(self.m))*dl.dx))
                print(" Energy: ", Fn)
                if self.it > 0:
                    print(" grad_F_du: ", grad_du)
                    print(" Curvature: ", H_grad)
                    print("\n")
            if rel_gn_u_norm < tol:
                if self.print_level > 0:
                    print("Terminate at iter #", self.it, " : gradient norm small.")
                break

            print("H_grad", H_grad)
            if H_grad > 0 and H_grad < tol:
                if self.print_level > 0:
                    print("Terminate at iter #", self.it, " : small curvature.")
                break
        
        state.assign(dl.project(self.u, self.U))
