import dolfin as dl
import math
import random as rand
import numpy as np
import matplotlib.pyplot as plt

dl.parameters["form_compiler"]["optimize"]     = True
dl.parameters["form_compiler"]["cpp_optimize"] = True
dl.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

#Class for initial condition
class InitialConditions(dl.UserExpression):
    """Class for initialize initial condition in the cartesian product space consists of two state function spaces"""
    def __init__(self, **kwargs):
        rand.seed(1)
        if dl.has_pybind11():
            super().__init__(**kwargs)
    def eval(self, values, x):
        # Assigning initial condition of u as u0 = m+Cr
        m = 0
        C = 0.5
        r = (2*rand.random()-1)
        values[0] = m + C*r
        values[1] = 0.0
    def value_shape(self):
        return (2,)

# Class for poisson equation solve
class poisson_solve():
    def __init__(self, Uh, Zh):
        """Class for solving the possion equation with homogeneous Neumann boundary. The rhs is projected in to the zero average space and the solution is in the zero average space.
            """
        self.Uh = Uh
        self.Zh = Zh
        
        u_trial = dl.TrialFunction(Uh)
        u_test = dl.TestFunction(Uh)
        z_trial = dl.TrialFunction(Zh)
        (w_trial, lam_trial) = dl.split(z_trial)
        z_test = dl.TestFunction(Zh)
        (w_test, lam_test) = dl.split(z_test)
        a_varf = dl.inner(dl.grad(w_trial), dl.grad(w_test))*dl.dx + w_trial*lam_test*dl.dx + lam_trial*w_test*dl.dx
        self.A = dl.assemble(a_varf)
        
        self.M = dl.assemble(u_trial*u_test*dl.dx)
        
        self.solver = dl.PETScLUSolver()
        self.solver.set_operator(self.A)
    
    def init_vector(self, v, dim):
        """Initialized vector corresponing to space Uh"""
        self.M.init_vector(v,dim)
    
    def vector_U2Z(self, v, y):
        
        y.zero()
        w_temp = dl.Function(self.Uh)
        w_temp.vector()[:] = v
        z_test = dl.TestFunction(self.Zh)
        (w_test, lam_test) = dl.split(z_test)
        y.axpy(1., dl.assemble(w_temp*w_test*dl.dx))
    
    def vector_Z2U(self, v, y):
        
        y.zero()
        z_temp = dl.Function(self.Zh)
        z_temp.vector().zero()
        z_temp.vector().axpy(1., v)
        y.axpy(1., z_temp.split(deepcopy=True)[0].vector())
    
    def solve(self, v, y):
        
        rhs, sol = dl.Vector(), dl.Vector()
        self.A.init_vector(rhs, 1)
        self.A.init_vector(sol, 1)
        self.vector_U2Z(y, rhs)
        self.solver.solve(sol, rhs)
        self.vector_Z2U(sol, v)

class ok_min():
    """Class for solving the problem of minimizing of Ohta-Kawasaki energy functional and providing the QoI of the solution."""
    
    def __init__(self, m, print_level):
        """Initialize the forward solver with provided mass average, m. Create the mesh and the function spaces"""
        
        dl.parameters["linear_algebra_backend"] = "PETSc"
        dl.parameters["form_compiler"]["cpp_optimize"] = True
        dl.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"
        
        #Initialize iteration count
        self.it = 0
        
        # Assign variables
        self.m      = m
        self.print_level = print_level
        
        # Initialize parameter objects
        self.kappa  = dl.Constant(1.0)
        self.eps    = dl.Constant(0.01)
        self.sigma  = dl.Constant(100)
        
        #Define element numbers and domain size
        N1 = 100
        N2 = 100
        l1 = 1.
        l2 = 1.
        
        # Create mesh
        self.mesh = dl.RectangleMesh(dl.Point(0,0), dl.Point(l1,l2), N1, N2)
        
        # Calculate volume
        self.vol = dl.assemble(dl.Constant(1.0)*dl.dx(self.mesh))
        
        # Define Finite Elements
        P1 = dl.FiniteElement("Lagrange", self.mesh.ufl_cell(), 1)
        R = dl.FiniteElement("R", self.mesh.ufl_cell(), 0)
        
        #Define the function space for the mixed problem and poisson problem
        ME1 = dl.MixedElement([P1,P1])
        ME2 = dl.MixedElement([P1,R])
        self.Z1 = dl.FunctionSpace(self.mesh, ME1)
        self.Z2 = dl.FunctionSpace(self.mesh, ME2)
        
        # Define the space of the state
        self.U = dl.FunctionSpace(self.mesh, P1)
            
        # Define the test/trial functions in the mixed space
        z_test = dl.TestFunction(self.Z1)
        (u_test, mu_test) = dl.split(z_test)
        z_trial = dl.TrialFunction(self.Z1)
        (u_trial, mu_trial) = dl.split(z_trial)
            
        # Define the test/trial functions
        v_test = dl.TestFunction(self.U)
        v_trial = dl.TrialFunction(self.U)
        
        # Define the mass matrix in state
        self.M = dl.assemble(v_trial*v_test*dl.dx)
            
        # Initialize forward solver
        self.psolver = poisson_solve(self.U, self.Z2)
        
        # Define functions
        self.z  = dl.Function(self.Z1)
        (self.u, mu) = dl.split(self.z)
        self.w  = dl.Function(self.U)
        self.mu_true = dl.Function(self.U)
        
        # Double well potential and its derivative
        f = self.kappa*(dl.Constant(1.0)-self.u**2)**2
        dfdu = dl.Constant(4)*self.kappa*(self.u**2-dl.Constant(1.0))*self.u
        df2du2 = dl.Constant(8)*self.kappa*self.u**2 + dl.Constant(4)*self.kappa*(self.u**2-dl.Constant(1.0))
        df2du2_GN = dl.Constant(8)*self.kappa*self.u**2
        df2du2_scaled = dl.Constant(8)*self.kappa*self.u**2 + dl.Constant(0.5*4)*self.kappa*(self.u**2-dl.Constant(1.0))
     
        # Define energy variational form
        self.F_varf = f*dl.dx + dl.Constant(0.5)*self.eps**2*dl.inner(dl.grad(self.u),dl.grad(self.u))*dl.dx + dl.Constant(0.5)*self.sigma*dl.inner(dl.grad(self.w),dl.grad(self.w))*dl.dx
        
        # Define rhs for solving mu
        self.mu_rhs_varf = dfdu*v_test*dl.dx + self.eps**2*dl.inner(dl.grad(self.u),dl.grad(v_test))*dl.dx
        
        # Define Gradient weak form in mixed space
        self.grad_varf = dl.inner(dl.grad(self.mu_true), dl.grad(u_test))*dl.dx + self.sigma*(self.u-dl.Constant(self.m))*u_test*dl.dx

        # Define Gradient weak form in state space
        self.grad_u_varf = dl.inner(dl.grad(self.mu_true), dl.grad(v_test))*dl.dx + self.sigma*(self.u-dl.Constant(self.m))*v_test*dl.dx
        
        # Define Energy Gradient
        self.grad_F_varf = dfdu*v_test*dl.dx + self.eps**2*dl.inner(dl.grad(self.u), dl.grad(v_test))*dl.dx + self.sigma*self.w*v_test*dl.dx
        
        # Define Hessian weak form components
        H_uu = self.sigma*u_trial*u_test*dl.dx

        H_umu = dl.inner(dl.grad(mu_trial), dl.grad(u_test))*dl.dx

        H_muu = -df2du2*u_trial*mu_test*dl.dx - self.eps**2*dl.inner(dl.grad(u_trial), dl.grad(mu_test))*dl.dx

        H_GN_muu = -df2du2_GN*u_trial*mu_test*dl.dx - self.eps**2*dl.inner(dl.grad(u_trial), dl.grad(mu_test))*dl.dx

        H_scaled_muu = -df2du2_scaled*u_trial*mu_test*dl.dx - self.eps**2*dl.inner(dl.grad(u_trial), dl.grad(mu_test))*dl.dx

        H_mumu = mu_trial*mu_test*dl.dx

        # Define Hessian weak form
        self.H_varf = H_uu + H_umu + H_muu + H_mumu
        self.H_GN_varf = H_uu + H_umu + H_GN_muu + H_mumu
        self.H_scaled_varf = H_uu + H_umu + H_scaled_muu + H_mumu
        
        # Define Hessian matrix for mu_hat
        self.mu_hat_varf = df2du2*v_trial*v_test*dl.dx + self.eps**2*dl.inner(dl.grad(v_trial), dl.grad(v_test))*dl.dx
    
    def solve_fwd(self, theta):
        """Solve forward problem with given vetcor of parameters, theta, and return the solution and its corresponding function space"""
        
        # Assign model parameter
        self.kappa.assign(theta[0])
        self.eps.assign(theta[1])
        self.sigma.assign(theta[2])
        
        # Assign initial condition for z
        z_init = InitialConditions(degree=1)
        self.z.interpolate(z_init)
        
        # Solve the poisson problem for w
        self.psolver.solve(self.w.vector(), dl.project(self.u, self.U).vector())
        
        # Solve for mu
        mu_rhs = dl.assemble(self.mu_rhs_varf)
        dl.solve(self.M, self.mu_true.vector(), mu_rhs)
        
        # Assemble gradients
        gn = dl.assemble(self.grad_varf)
        gn_u = dl.assemble(self.grad_u_varf)
        gn_F = dl.assemble(self.grad_F_varf)
        
        # Evaluete energy
        Fn = dl.assemble(self.F_varf)
        
        # Initialize the update vectors/functions (dz,du,dw)
        dz = dl.Function(self.Z1)
        du, dw= dl.Vector(), dl.Vector()
        self.M.init_vector(du, 1)
        self.M.init_vector(dw, 1)
        
        # Define storage functions
        z_prev = dl.Function(self.Z1)
        w_prev = dl.Function(self.U)
        
        # Define the gradient represntation vector
        g = dl.Vector()
        self.M.init_vector(g, 1)
        
        # Solve for L2 Norm for gradient
        dl.solve(self.M, g, gn_u)
        gn_u_norm = g.inner(gn_u)
        
        # Solve for curvature in gradient direction
        mu_hat = dl.assemble(self.mu_hat_varf)
        H_grad = g.inner(mu_hat*g) + theta[2]*gn_F.inner(g)
        
        # Newton iteration parameters
        self.it = 0
        max_it = 500
        max_backtrack = 50
        tol = 1e-6
        c_armijo = 1.e-4
        
        # Storage of solution
        if self.print_level > 2:
            file = dl.File("data/u.pvd","compressed")
            file << (self.z.split()[0], 0.)
        
        # Starting Newton iteration
        for self.it in range(max_it):
            
            # Backtracking double well potential
            for gn_iter in range(3):
                
                if gn_iter == 2 or self.it < 5:
                    Hn = dl.assemble(self.H_GN_varf)
                    if self.print_level > 1:
                        print("Switch to Gauss-Newton Hessian...")
                elif gn_iter == 1:
                    Hn = dl.assemble(self.H_scaled_varf)
                    if self.print_level > 1:
                        print("Switch to scaled Hessian...")
                else:
                    Hn = dl.assemble(self.H_varf)
                
                dl.solve(Hn, dz.vector(), -gn)
                du.zero()
                du.axpy(1., dz.split(deepcopy = True)[0].vector())
                grad_du = gn_F.inner(du)
                if grad_du <= 0.0 or self.it < 5:
                    break

            #Backtrack
            z_prev.assign(self.z)
            w_prev.assign(self.w)
            alpha = 1.
            bk_converged = False
            self.psolver.solve(dw, du)
            if self.it > 0:
                for j in range(max_backtrack):
                    self.z.assign(z_prev)
                    self.w.assign(w_prev)
                    self.z.vector().axpy(alpha, dz.vector())
                    self.w.vector().axpy(alpha, dw)
                    Fnext = dl.assemble(self.F_varf)
                    if Fnext < Fn + alpha*c_armijo*grad_du:
                        Fn = Fnext
                        bk_converged = True
                        if self.print_level > 1:
                            print("Backtracking converges with alpha = ", alpha)
                        break
                    alpha *= 0.5
            if not bk_converged:
                alpha = 1.0
                self.z.assign(z_prev)
                self.w.assign(w_prev)
                self.z.vector().axpy(1, dz.vector())
                if self.it > 0:
                    self.w.vector().axpy(1., dw)
                else:
                    self.psolver.solve(self.w.vector(), dl.project(self.u, self.U).vector())
                    Fn = dl.assemble(self.F_varf)
                if self.print_level > 1:
                    print("Backtracking does not converge.")

            # Solve for mu
            mu_rhs = dl.assemble(self.mu_rhs_varf)
            dl.solve(self.M, self.mu_true.vector(), mu_rhs)
                        
            # Assemble gradients
            gn = dl.assemble(self.grad_varf)
            gn_u = dl.assemble(self.grad_u_varf)
            gn_F = dl.assemble(self.grad_F_varf)
            
            # Solve for L2 Norm for gradient
            dl.solve(self.M, g, gn_u)
            gn_u_norm = g.inner(gn_u)

            # Solve for curvature in gradient direction
            mu_hat = dl.assemble(self.mu_hat_varf)
            H_grad = g.inner(mu_hat*g) + theta[2]*gn_F.inner(g)
            
            if self.print_level > 2:
                file << (self.z.split()[0], self.it+1.)
            if self.print_level > 1:
                print("iter #", self.it+1)
                print(" Residule L^2 norm: ", gn_u_norm)
                print(" Residule of mass average: ", dl.assemble((self.z.sub(0)-dl.Constant(self.m))*dl.dx))
                print(" Energy: ", Fn)
                if self.it > 0:
                    print(" grad_F_du: ", grad_du)
                    print(" Curvature: ", H_grad)
                print("\n")
            if gn_u_norm < tol:
                if self.print_level > 0:
                    print("Terminate at iter #", self.it, " : gradient norm small.")
                break
            if H_grad > 0 and H_grad < tol:
                if self.print_level > 0:
                    print("Terminate at iter #", self.it, " : small curvature.")
                break
    
    def pto(self):
        
        dl.plot(self.u, vmin = -1, vmax = 1)
        
        plt.show()
        
        u_dir = dl.Function(self.U)
        dir_hr = dl.Expression("-x[0]", degree = 1)
        u_dir.interpolate(dir_hr)
        
        qoi = []

        qoi.append(dl.assemble(dl.inner(dl.grad(self.u), dl.grad(self.u))**0.5*dl.dx)/(0.5*self.vol*(1-abs(self.m))))
        qoi.append(dl.assemble(abs(dl.inner(dl.grad(self.u), dl.grad(u_dir)))/dl.inner(dl.grad(self.u), dl.grad(self.u))**0.5*dl.dx)/self.vol)
        
        return qoi



solver = ok_min(m=0., print_level=3)

theta = [1., 0.01, 100]

solver.solve_fwd(theta)