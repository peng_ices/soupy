from __future__ import absolute_import, division, print_function

import dolfin as dl
dl.dx = dl.dx(metadata={'quadrature_degree':2, "representation":'uflacs'})
import numpy as np

path = "../../"

import sys
sys.path.append(path)
from soupy.utils.variables import STATE, PARAMETER, ADJOINT
from soupy.utils.vector2function import vector2Function


class QoIDiffusion:

    """
    misfit term of simulaation-observation at a few locations
    Q = ||O(u) - \bar{u}||^2
    O is the observation functional
    u is the state variable
    \bar{u} is the target
    """
    def __init__(self, mesh, Vh):
        """
        Constructor.
        INPUTS:
        - mesh: the mesh
        - Vh_STATE: the finite element space for the state variable
        """
        x = np.linspace(0.5, 1.5, 4)
        y = np.linspace(0.3, 0.7, 3)
        xv,yv = np.meshgrid(x,y)
        ubar = 3 - 4*(xv-1)**2 - 8*(yv-0.5)**2
        self.ubar = np.reshape(ubar, 12, 1)

        self.Obs = dict()
        for i in range(4):
            for j in range(3):
                obs = dl.Function(Vh[STATE]).vector()
                ps = dl.PointSource(Vh[STATE], dl.Point(x[i], y[j]), 1.)
                ps.apply(obs)
                self.Obs[i*3+j] = obs

        self.x = [dl.Function(Vh[i]).vector() for i in range(4)]
        self.Vh = Vh

    def form(self, x):
        """
        Build the weak form of the qoi
        :param x:
        :return:
        """
        x_fun = [None] * len(x)
        for i in range(len(x)):
            x_fun[i] = vector2Function(x[i], self.Vh[i])

        # this is not a correct representation, just a place holder
        v = dl.TestFunction(self.Vh[STATE])
        return x_fun[STATE] * dl.dx

    def eval(self, x):
        """
        Evaluate the quantity of interest at a given point in the state and
        parameter space.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        """
        QoI = 0
        for i in range(12):
            QoI += (self.Obs[i].inner(x[STATE]) - self.ubar[i])**2

        return QoI

    def adj_rhs(self,x,rhs):
        """
        The right hand for the adjoint problem (i.e. the derivative of the Lagrangian funtional
        with respect to the state u).

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - rhs: FEniCS vector to store the rhs for the adjoint problem.
        """
        ### rhs = - df/dstate
        self.grad_state(x, rhs)
        rhs *= -1

    def grad_state(self,x,g):
        """
        The partial derivative of the qoi with respect to the state variable.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - g: FEniCS vector to store the gradient w.r.t. the state.
        """
        g.zero()
        for i in range(12):
            g.axpy(2*(self.Obs[i].inner(x[STATE]) - self.ubar[i]), self.Obs[i])

    def grad_parameter(self, x, g):
        """
        The partial derivative of the qoi with respect to the parameter variable.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - g: FEniCS vector to store the gradient w.r.t. the parameter.
        """
        g.zero()

    def grad_optimization(self, x, g):
        """
        The partial derivative of the qoi with respect to the optimization variable.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - g: FEniCS vector to store the gradient w.r.t. the optimization.
        """
        g.zero()

    def apply_ij(self,i,j, dir, out):
        """
        Apply the second variation \delta_ij (i,j = STATE,PARAMETER,OPTIMIZATION) of the q.o.i. in direction dir.

        INPUTS:
        - i,j integer (STATE=0, PARAMETER=1, OPTIMIZATION=3) which indicates with respect to which variables differentiate
        - dir the direction in which to apply the second variation
        - out: FEniCS vector to store the second variation in the direction dir.

        NOTE: setLinearizationPoint must be called before calling this method.
        """
        out.zero()
        if i == STATE and j == STATE:
            for n in range(12):
                out.axpy(2*self.Obs[n].inner(dir), self.Obs[n])

    def apply_ijk(self,i,j,k,dir1,dir2, out):
        """
        Apply the third variation \delta_ijk (i,j,k = STATE,PARAMETER,OPTIMIZATION) of the q.o.i. in direction dir1, dir2

        INPUTS:
        - i,j,k integer (STATE=0, PARAMETER=1, OPTIMIZATION=3) which indicates with respect to which variables differentiate
        - dir1,dir2 the direction in which to apply the second and third variations
        - out: FEniCS vector to store the second variation in the direction dir.

        NOTE: setLinearizationPoint must be called before calling this method.
        """
        out.zero()

    def setLinearizationPoint(self, x):
        """
        Specify the linearization point for computation of the variations in method apply_ij and apply_ijk.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        """
        for i in range(len(x)):
            self.x[i].zero()
            self.x[i].axpy(1., x[i])
