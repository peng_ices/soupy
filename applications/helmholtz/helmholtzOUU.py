### optimization under uncertainty constrained by Helmholtz model

from construction import *
from shutil import copyfile

################### 6. save data ##########################################
def initialdata(z):

    cost.costValue(z)

    # for i in range(N_pde):
    x = cost.x
    x_fun = vector2Function(x, Vh_STATE, name="state")
    ur_scattered, ui_scattered = x_fun.split()

    if plotFlag:
        dl.plot(ur_scattered, title='init state real')
        dl.plot(ui_scattered, title='init state imag')

    # ur_incident, ui_incident = u_incident[i][0], u_incident[i][1]
    u_total = dl.Function(Vh_STATE, name="state")
    ur_total, ui_total = u_total.split()
    ur_total, ui_total = dl.interpolate(ur_total, Vh_STATE_sub), dl.interpolate(ui_total, Vh_STATE_sub)
    ur_scattered, ui_scattered = dl.interpolate(ur_scattered, Vh_STATE_sub), dl.interpolate(ui_scattered, Vh_STATE_sub)
    ur_total.vector().axpy(1.0, ur_scattered.vector())
    ur_total.vector().axpy(1.0, ur_incident.vector())
    ui_total.vector().axpy(1.0, ui_scattered.vector())
    ui_total.vector().axpy(1.0, ui_incident.vector())

    if dlversion() <= (1,6,0):
        filename = "data/ur_init_scattered.xdmf"
        dl.File(comm, filename) << ur_scattered
        filename = "data/ui_init_scattered.xdmf"
        dl.File(comm, filename) << ui_scattered
        filename = "data/ur_init_total.xdmf"
        dl.File(comm, filename) << ur_total
        filename = "data/ui_init_total.xdmf"
        dl.File(comm, filename) << ui_total
    else:
        filename = "data/ur_init_scattered.xdmf"
        xf = dl.XDMFFile(comm, filename)
        xf.write(ur_scattered)
        filename = "data/ui_init_scattered.xdmf"
        xf = dl.XDMFFile(comm, filename)
        xf.write(ui_scattered)
        filename = "data/ur_init_total.xdmf"
        xf = dl.XDMFFile(comm, filename)
        xf.write(ur_total)
        filename = "data/ui_init_total.xdmf"
        xf = dl.XDMFFile(comm, filename)
        xf.write(ui_total)

    noise = dl.Vector()
    sample = dl.Vector()
    prior.init_vector(noise, "noise")
    prior.init_vector(sample, 1)
    noise_size = noise.get_local().shape[0]

    for i in range(4):
        sample = dl.Vector()
        noise.set_local(np.random.normal(0, 1, noise_size))
        prior.sample(noise, sample, add_mean=True)
        sample_fun = vector2Function(sample,Vh[PARAMETER], name="sample")
        sample_fun = dl.project(sample_fun*chi_design, Vh_PARAMETER)
        sample_fun = vector2Function(sample_fun.vector(), Vh[PARAMETER], name="sample")
        if plotFlag:
            dl.plot(sample_fun)

        filename = 'data/sample_'+str(i)+'.xdmf'
        if dlversion() <= (1,6,0):
            dl.File(comm,filename) << sample_fun
        else:
            xf = dl.XDMFFile(comm, filename)
            xf.write(sample_fun)

def savedata(type):

    # for i in range(N_pde):
    if type is 'saa':
        x_fun = vector2Function(cost.x[0], Vh_STATE, name="state")
    else:
        x_fun = vector2Function(cost.x, Vh_STATE, name="state")
    z_fun = vector2Function(cost.z, Vh_OPTIMIZATION, name="design")

    ur_scattered, ui_scattered = x_fun.split()

    if plotFlag:
        dl.plot(ur_scattered, title='optimal state real')
        dl.plot(ui_scattered, title='optimal state imag')
        dl.plot(z_fun, title="optimal control")
        dl.plot(kopt, title="optimal wavenumber")

    # ur_incident, ui_incident = u_incident[i][0], u_incident[i][1]
    u_total = dl.Function(Vh_STATE, name="state")
    ur_total, ui_total = u_total.split()
    ur_total, ui_total = dl.interpolate(ur_total, Vh_STATE_sub), dl.interpolate(ui_total, Vh_STATE_sub)
    ur_scattered, ui_scattered = dl.interpolate(ur_scattered, Vh_STATE_sub), dl.interpolate(ui_scattered, Vh_STATE_sub)
    ur_total.vector().axpy(1.0, ur_scattered.vector())
    ur_total.vector().axpy(1.0, ur_incident.vector())
    ui_total.vector().axpy(1.0, ui_scattered.vector())
    ui_total.vector().axpy(1.0, ui_incident.vector())

    if dlversion() <= (1,6,0):
        dl.File(comm, "data/"+type+"/ur_opt_scattered.xdmf") << ur_scattered
        dl.File(comm, "data/"+type+"/ui_opt_scattered.xdmf") << ui_scattered
        dl.File(comm, "data/"+type+"/ur_opt_total.xdmf") << ur_total
        dl.File(comm, "data/"+type+"/ui_opt_total.xdmf") << ui_total
    else:
        xf = dl.XDMFFile(comm, "data/"+type+"/ur_opt_scattered.xdmf")
        xf.write(ur_scattered)
        xf = dl.XDMFFile(comm, "data/"+type+"/ui_opt_scattered.xdmf")
        xf.write(ui_scattered)
        xf = dl.XDMFFile(comm, "data/"+type+"/ur_opt_total.xdmf")
        xf.write(ur_total)
        xf = dl.XDMFFile(comm, "data/"+type+"/ui_opt_total.xdmf")
        xf.write(ui_total)

    z_fun = vector2Function(cost.z, Vh_OPTIMIZATION, name="design")
    z1, z2, z3, z4 = z_fun.split()
    z1, z2, z3, z4 = dl.interpolate(z1, Vh_OPTIMIZATION_sub), dl.interpolate(z2, Vh_OPTIMIZATION_sub), \
                     dl.interpolate(z3, Vh_OPTIMIZATION_sub), dl.interpolate(z4, Vh_OPTIMIZATION_sub)
    output_file = dl.HDF5File(comm, "data/" + type + "/z_opt_load.h5", "w")
    output_file.write(z_fun, "z_opt")
    output_file.close()

    if dlversion() <= (1, 6, 0):
        dl.File(comm, "data/" + type + "/z1_opt.xdmf") << z1
        # dl.File(comm, "data/" + type + "/z2_opt.xdmf") << z2
        # dl.File(comm, "data/" + type + "/z3_opt.xdmf") << z3
        # dl.File(comm, "data/" + type + "/z4_opt.xdmf") << z4
    else:
        xf = dl.XDMFFile(comm, "data/" + type + "/z1_opt.xdmf")
        xf.write(z1)
        # xf = dl.XDMFFile(comm, "data/" + type + "/z2_opt.xdmf")
        # xf.write(z2)
        # xf = dl.XDMFFile(comm, "data/" + type + "/z3_opt.xdmf")
        # xf.write(z3)
        # xf = dl.XDMFFile(comm, "data/" + type + "/z4_opt.xdmf")
        # xf.write(z4)

    if mpi_rank == 0:
        print("optimization result", opt_result)
        print("func_ncalls, grad_ncalls, hess_calls = ", cost.func_ncalls, cost.grad_ncalls, cost.hess_ncalls)

        data = dict()
        data['opt_result'] = opt_result
        data['tobj'] = cost.tobj
        data['tgrad'] = cost.tgrad
        data['trand'] = cost.trand
        data['func_ncalls'] = cost.func_ncalls
        data['grad_ncalls'] = cost.grad_ncalls
        data['hess_ncalls'] = cost.hess_ncalls
        # data['cg_ncalls'] = cost.cg_ncalls

        data['lin_mean'] = cost.lin_mean
        data['lin_diff_mean'] = cost.lin_diff_mean
        data['lin_fval_mean'] = cost.lin_fval_mean
        data['lin_var'] = cost.lin_var
        data['lin_diff_var'] = cost.lin_diff_var
        data['lin_fval_var'] = cost.lin_fval_var

        data['quad_mean'] = cost.quad_mean
        data['quad_var'] = cost.quad_var
        data['quad_diff_mean'] = cost.quad_diff_mean
        data['quad_fval_mean'] = cost.quad_fval_mean
        data['quad_diff_var'] = cost.quad_diff_var
        data['quad_fval_var'] = cost.quad_fval_var

        pickle.dump( data, open( "data/"+type+"/data.p", "wb" ) )

        copyfile("iterate.dat", "data/"+type+"/iterate.dat")

################# 7. Solve the optimization algorithms #####################

# bounds = [(0,1.)]
# for i in range(z.shape[0]-1):
#     bounds.append((0,1.))

bounds = None
if optMethod is 'home_bfgs':
    maxiter = bfgs_ParameterList["max_iter"]
elif optMethod is 'home_ncg':
    maxiter = ncg_ParameterList["max_iter"]
else:
    maxiter = 32

def optimization(cost, z):

    if check_gradient:
        cost.checkGradient(z, mpi_comm, plotFlag=False)
    if check_hessian:
        cost.checkHessian(z, mpi_comm, plotFlag=False)

    if optMethod is 'home_bfgs':
        bfgs_Solver = BFGS(cost, bfgs_ParameterList)
        h0inv = H0inv(penalization)
        # z = pde.generate_optimization()
        opt_result = bfgs_Solver.solve(z, h0inv)
    elif optMethod is 'home_ncg':
        ncg_Solver = ReducedSpaceNewtonCG(cost, ncg_ParameterList)
        # z = pde.generate_optimization()
        opt_result = ncg_Solver.solve(z)
    elif optMethod is 'sci_bfgs':
        opt_result = fmin_l_bfgs_b(func=cost.costValue, x0=z, fprime=cost.costGradient,
                        disp=1, pgtol=1e-5, maxiter=maxiter, factr=1e12, bounds = bounds)
    elif optMethod is 'fmin_ncg':
        opt_result = fmin_ncg(f=cost.costValue, x0=z, fprime=cost.costGradient, fhess_p=cost.costHessian,
                        avextol=1e-04, maxiter=maxiter, full_output=1, disp=1, retall=1)

    return cost, opt_result

factor_beta = 2
factor_gamma = 4
num_iteration = 4

from mpi4py import MPI
comm_mpi4py = MPI.COMM_WORLD


def optimization_update(cost):
    c_beta = parameter["c_beta"]
    c_gamma = parameter["c_gamma"]
    for i in range(num_iteration):
        c_beta *= factor_beta
        c_gamma *= factor_gamma
        cost.update(c_beta, c_gamma)
        cost, opt_result = optimization(cost, cost.z.copy())

        print("update parameters c_beta = ", c_beta, " and c_gamma = ", c_gamma)
        print("optimization result", opt_result)
        print("func_ncalls, grad_ncalls, hess_calls = ", cost.func_ncalls, cost.grad_ncalls, cost.hess_ncalls)

    return cost, opt_result

if constant_run:
    ######## Zero-th approximation ########################
    cost = ChanceConstraintConstant(parameter, Vh, pde, qoi_objective, qoi_constraint, prior, penalization, tol=1e-10)

    # generate and save initial data
    z = pde.generate_optimization()
    if optMethod is not 'home_bfgs' and optMethod is not 'home_ncg':
        z = np.zeros(z.size())

    initialdata(z)

    cost, opt_result = optimization(cost, z)

    cost, opt_result = optimization_update(cost)

    savedata("constant")

if linear_run:
    ######## Linear approximation ########################
    cost = ChanceConstraintLinear(parameter, Vh, pde, qoi_objective, qoi_constraint,  prior, penalization, tol=1e-10)

    z = pde.generate_optimization()
    if optMethod is not 'home_bfgs' and optMethod is not 'home_ncg':
        z = np.zeros(z.size())

    cost, opt_result = optimization(cost, z)

    cost, opt_result = optimization_update(cost)

    savedata("linear")

if quadratic_run:
    ######### Quadratic approximation ########################
    cost = ChanceConstraintQuadratic(parameter, Vh, pde, qoi_objective, qoi_constraint, prior, penalization, tol=1e-10)

    z = pde.generate_optimization()
    if optMethod is not 'home_bfgs' and optMethod is not 'home_ncg':
        z = np.zeros(z.size())

    cost, opt_result = optimization(cost, z)

    cost, opt_result = optimization_update(cost)

    savedata("quadratic")

if saa_run:
    ######## sample average approximation ########################
    cost = ChanceConstraintSAA(parameter, Vh, pde, qoi_objective, qoi_constraint, prior, penalization, tol=1e-10)

    z = pde.generate_optimization()
    if optMethod is not 'home_bfgs' and optMethod is not 'home_ncg':
        z = np.zeros(z.size())

    cost, opt_result = optimization(cost, z)

    cost, opt_result = optimization_update(cost)

    savedata("saa")

if plotFlag:
    dl.interactive()