from __future__ import absolute_import, division, print_function

import math
import numpy as np
import pylab as plt
import dolfin as dl
dl.dx = dl.dx(metadata={'quadrature_degree':2, "representation":'uflacs'})
dl.set_log_active(False)
# import mshr as mh
import pickle
from qoiThermalFin import QoIObjective, QoIConstraint

# path = ""
path = "../../"

import sys
sys.path.append(path)
from soupy import *

# # samples used to compute mean square error
N_mse = 10

# choose mesh size, [1, 2, 3, 4, 5]
meshsize = 2

optMethod = 'home_bfgs'
# optMethod = 'home_ncg'
# optMethod = 'scipy_bfgs'
# optMethod = 'fmin_ncg'

if optMethod is 'home_bfgs':
    bfgs_ParameterList = BFGS_ParameterList()
    bfgs_ParameterList["max_iter"] = 16
    bfgs_ParameterList["LS"]["max_backtracking_iter"] = 10
elif optMethod is 'home_ncg':
    ncg_ParameterList = ReducedSpaceNewtonCG_ParameterList()
    ncg_ParameterList["globalization"] = "LS"
    ncg_ParameterList["max_iter"] = 16
    ncg_ParameterList["LS"]["max_backtracking_iter"] = 10
elif optMethod is 'scipy_bfgs':
    from scipy.optimize import fmin_l_bfgs_b

plotFlag = False

############ 0 parameters to config ###############

## use Monte Carlo correction True/False
correction = False

# active = [0, 0, 1, 0]
active = [1, 1, 1, 1]
# active = [0, 1, 0, 0]

# run the constant approximation True/False
constant_run = active[0]
# run the linear approximation True/False
linear_run = active[1]
# run the quadratic approximation True/False
quadratic_run = active[2]
# run the SAA approximation True/False
saa_run = active[3]

# check gradient True/False
check_gradient = False
# check Hessian True/False
check_hessian = False

if correction:
    parameter = pickle.load(open('data/parameter.p', 'rb'))
    parameter["correction"] = correction
else:
    parameter = dict()
    # with Monte Carlo correction or not
    parameter["correction"] = correction
    # optimization method
    parameter["optMethod"] = optMethod
    # mesh size
    parameter["meshsize"] = meshsize
    parameter["optType"] = 'field'
    # number of eigenvalues for trace estimator
    parameter['N_tr'] = 10
    # number of samples for Monte Carlo correction
    parameter['N_mc'] = 100
    # regularization coefficient
    parameter['alpha'] = 1.e-5  # alpha*R(z)
    # variance coefficient
    parameter['beta'] = 0.     # E[Q] + beta Var[Q]
    # prior covariance, correlation length prop to gamma/delta
    parameter['delta'] = 1    # delta*Identity
    parameter['gamma'] = 0.1    # gamma*Laplace

    # number of samples for Monte Carlo evaluation of chance/probability P(f \leq 0)
    parameter['N_cc'] = 1000
    # chance c_alpha in the chance constraint P(f \leq 0) \geq c_alpha
    parameter['c_alpha'] = 0.05
    # smoothing parameter of indicator function I_{(-\infty, 0]}(x) \approx 1 / ( 1 + \exp(-2 c_\beta x) )
    parameter['c_beta'] = 8.
    # penalty parameter of the inequality chance constraint c_gamma/2 (\max(0, x))^2
    parameter['c_gamma'] = 64.

    pickle.dump(parameter, open('data/parameter.p','wb'))

################### 1. Define the Geometry ###########################

dl.parameters["ghost_mode"] = "shared_facet"

# 1. Define the Geometry
filename = "mesh/thermalfin"+str(meshsize)+".xml"
mesh = dl.Mesh(filename)

boundary_markers = dl.FacetFunction('size_t', mesh)
boundary_markers.set_all(0)

class BoundaryBottom(dl.SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and dl.near(x[1], 0, 1e-15)

bxb = BoundaryBottom()
bxb.mark(boundary_markers, 1)

ds = dl.Measure('ds', domain=mesh, subdomain_data=boundary_markers)

comm = mesh.mpi_comm()
mpi_comm = mesh.mpi_comm()
if hasattr(mpi_comm, "rank"):
    mpi_rank = mpi_comm.rank
    mpi_size = mpi_comm.size
else:
    mpi_rank = 0
    mpi_size = 1

#################### 2. Define optimization PDE problem #######################

Vh_STATE = dl.FunctionSpace(mesh, 'CG', 1)
Vh_PARAMETER = dl.FunctionSpace(mesh, "CG", 1)
Vh_OPTIMIZATION = dl.FunctionSpace(mesh, "CG", 1)
Vh = [Vh_STATE, Vh_PARAMETER, Vh_STATE, Vh_OPTIMIZATION]

zbc = dl.TestFunction(Vh_OPTIMIZATION)
zvec = dl.assemble(zbc*ds(0))
vertices_on_boundary = (zvec > 1e-15)

# bc = dl.DirichletBC(Vh_OPTIMIZATION, 1.0, boundary_markers, 0)
# zbc = dl.Function(Vh_OPTIMIZATION)

# plt.figure(0)
# c = dl.plot(zbc, mode='color')
# plt.colorbar(c)

# bc.apply(zbc.vector())
# # Get vertices sitting on boundary
# d2v = dl.dof_to_vertex_map(Vh_OPTIMIZATION)
# vertices_on_boundary = d2v[zbc.vector() == 1.0]

# print("vertices_on_boundary", vertices_on_boundary, len(vertices_on_boundary),"d2v", d2v, len(d2v))

flux = dl.Constant(1.0)
uref = dl.Constant(25.0)
def residual(u, m, p, z):

    form = dl.exp(m)*dl.inner(dl.nabla_grad(u), dl.nabla_grad(p))*dl.dx - flux*p*ds(1) + z*(u-uref)*p*ds(0)

    return form

bcs = []
bcs0 = []

pde = ControlPDEProblem(Vh, residual, bcs, bcs0, is_fwd_linear=True)

################## 3. Define the quantity of interest (QoI) ############

qoi_objective = QoIObjective(mesh, Vh, ds)

qoi_constraint = QoIConstraint(mesh, Vh, ds)

################## 4. Define Penalization term ############################

# penalization = L2PenalizationFiniteDimension(Vh[OPTIMIZATION], dl.dx, parameter["alpha"])
penalization = L2Penalization(Vh[OPTIMIZATION], ds(1), parameter["alpha"])

################## 5. Define the prior ##################### ##############
anis_diff = dl.Expression(code_AnisTensor2D, degree=2)
anis_diff.theta0 = 1.
anis_diff.theta1 = 1.
anis_diff.alpha = 0.*np.pi/4

# def true_model(Vh, gamma, delta, anis_diff):
#     prior = BiLaplacianPrior(Vh, gamma, delta, anis_diff)
#     noise = dl.Vector()
#     prior.init_vector(noise,"noise")
#     noise_size = noise.get_local().shape[0]
#     noise_true = np.random.randn(noise_size)
#     np.save("data/noise_true",noise_true)
#     noise_true = np.load("data/noise_true.npy")
#     noise.set_local( noise_true)
#     # noise = dl.Vector()
#     # prior.init_vector(noise,"noise")
#     # Random.normal(noise, 1., True)
#     atrue = dl.Vector()
#     prior.init_vector(atrue, 0)
#     prior.sample(noise,atrue)
#     return atrue

# atrue = true_model(Vh[PARAMETER], parameter["gamma"], parameter["delta"], anis_diff)
# atrue_fun = vector2Function(atrue, Vh_PARAMETER, name="sample")
# plt.figure(1)
# c = dl.plot(atrue_fun, mode='color')
# plt.colorbar(c)

# if correction:
#     ############ begin load atrue  #####################
#     atrue_array = pickle.load( open( "data/atrue_array.p", "rb" ) )
#     atrue_fun = dl.Function(Vh_PARAMETER, name="sample")
#     atrue = atrue_fun.vector()
#     atrue[:] = atrue_array[:]
#     ############ finish load atrue #####################
# else:
#     ########## begin create atrue ####################
#     gamma_atrue = 2
#     delta_atrue = 20

#     def true_model(Vh, gamma, delta, anis_diff):
#         prior = BiLaplacianPrior(Vh, gamma, delta, anis_diff)
#         noise = dl.Vector()
#         prior.init_vector(noise,"noise")
#         noise_size = noise.get_local().shape[0]
#         noise_true = np.random.randn(noise_size)
#         np.save("data/noise_true",noise_true)
#         noise_true = np.load("data/noise_true.npy")
#         noise.set_local( noise_true)
#         # noise = dl.Vector()
#         # prior.init_vector(noise,"noise")
#         # Random.normal(noise, 1., True)
#         atrue = dl.Vector()
#         prior.init_vector(atrue, 0)
#         prior.sample(noise,atrue)
#         return atrue

#     atrue = true_model(Vh[PARAMETER], gamma_atrue, delta_atrue, anis_diff)
#     atrue_fun = vector2Function(atrue, Vh_PARAMETER, name="sample")
#     plt.figure()
#     c = dl.plot(atrue_fun, mode='color')
#     plt.colorbar(c)
# #     dl.plot(atrue_fun)

# #     locations = np.array([[.2, .1], [.2, .9], [.5,.5], [.8, .1], [.8, .9]])
# #     pen = 2e1
# #     gamma_moll = 1.
# #     delta_moll = 1.
# #     prior = MollifiedBiLaplacianPrior(Vh[PARAMETER], gamma_moll, delta_moll, locations, atrue, anis_diff, pen)
# #     atrue = prior.mean
# #     atrue_fun = vector2Function(atrue, Vh_PARAMETER, name="sample")
# #     dl.plot(atrue_fun)

#     atrue_array = np.array(atrue.get_local())
#     pickle.dump( atrue_array, open( "data/atrue_array.p", "wb" ) )
#     dl.File("data/atrue.pvd") << atrue_fun
#     ########### finish create atrue ####################

# prior = BiLaplacianPrior(Vh[PARAMETER], parameter["gamma"], parameter["delta"], anis_diff, atrue)

prior = BiLaplacianPrior(Vh[PARAMETER], parameter["gamma"], parameter["delta"], anis_diff)

# u = dl.TrialFunction(Vh_STATE)
# p = dl.TestFunction(Vh_STATE)
# m = dl.Function(Vh_PARAMETER)
# m.vector().axpy(1., atrue)
# z = dl.Function(Vh_OPTIMIZATION)
# z.vector().set_local(z.vector().get_local()+0.005)
# form = residual(u,m,p,z)

# # flux = dl.Constant(1.0)
# # Biot = dl.Constant(0.005)
# # form = dl.inner(dl.nabla_grad(u), dl.nabla_grad(p))*dl.dx - flux*p*ds(1) + Biot*(u-25)*p*ds(0)
# a, L = dl.lhs(form), dl.rhs(form)

# u = dl.Function(Vh_STATE)
# bcs = []
# bcs0 = []

# dl.solve(a==L,u,bcs)

# plt.figure(2)
# d = dl.plot(u, mode='color')
# plt.colorbar(d)