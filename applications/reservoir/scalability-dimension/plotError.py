import matplotlib.pyplot as plt
import numpy as np
import pickle

plt.tick_params(axis='both', which='major', labelsize=12)
plt.tick_params(axis='both', which='minor', labelsize=12)

marker = ["b.-", "r.-", "g.-", "k.-", "m.-"]
marker_objective = ["b.--", "r.--", "g.--", "k.--", "m.--"]

dim = ["dim = 1,089", "dim = 4,225", "dim = 16,641", "dim = 66,049", "dim = 263,169"]

fig = plt.figure()
for iter in range(5):
    filename = "mesh"+str(iter+1)+"/data/d_constraint.p"
    d_constraint = pickle.load(open(filename, "r"))
    plt.semilogy(np.abs(d_constraint), marker[iter], label=dim[iter])
    # plt.semilogy(np.abs(d_objective), marker_objective[iter])
    # plt.close()
plt.xlabel("$n$", fontsize=16)
plt.ylabel("$|\lambda_n|$", fontsize=16)
plt.legend(loc="best", fontsize=12)
filename = "figure/scalability-dimension.pdf"
fig.savefig(filename, format='pdf')
