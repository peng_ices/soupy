from construction import *
import matplotlib.pyplot as plt

x = [dl.Function(Vh[i]).vector() for i in range(4)]
state = dl.Function(Vh_STATE).vector()

filename = "data/quadratic/data.p"
data = pickle.load(open(filename, "r"))
z = data["opt_result"][0]

x[OPTIMIZATION].set_local(z)
x[PARAMETER].set_local(atrue.get_local())

pde.solveFwd(state, x)

state_fun = dl.Function(Vh_STATE)
state_fun.vector().set_local(state.get_local())

meshsize = 1
nx = 32*2**(meshsize-1)
ny = 32*2**(meshsize-1)
mesh = dl.RectangleMesh(dl.Point(0., 0.), dl.Point(1., 1.), nx, ny)

Vh_STATE = dl.FunctionSpace(mesh, 'CG', 1)
Vh_PARAMETER = dl.FunctionSpace(mesh, "CG", 1)
state_fun = dl.interpolate(state_fun, Vh_STATE)
atrue_fun = dl.interpolate(atrue_fun, Vh_PARAMETER)

velocity = -dl.grad(state_fun)*dl.exp(atrue_fun)
dl.plot(velocity)

x = np.linspace(0.25, 0.75, n1d)
y = np.linspace(0.25, 0.75, n1d)
xv,yv = np.meshgrid(x, y)
plt.plot(xv[:], yv[:], "bo")
plt.savefig("figure/flow.pdf")

