from __future__ import absolute_import, division, print_function

import math
import numpy as np
import dolfin as dl
dl.dx = dl.dx(metadata={'quadrature_degree':2, "representation":'uflacs'})
dl.set_log_active(False)

# import mshr as mh
import pickle
from qoiHelmholtz import QoIHelmholtz

# path = ""
path = "../../../../"

import sys
sys.path.append(path)
from soupy import *

# # samples used to compute mean square error
N_mse = 10

# number of PDEs
N_pde = 1

# choose mesh size, [1, 2, 3, 4, 5]
meshsize = 2

geometry = 'b2_hole_large'

filename = "../../mesh/"+geometry+str(meshsize)+".xml"
mesh = dl.Mesh(filename)
filename = "../../mesh/"+geometry+str(meshsize)+"_physical_region.xml"
subdomains = dl.MeshFunction("size_t", mesh, filename)
filename = "../../mesh/"+geometry+str(meshsize)+"_facet_region.xml"
boundaries = dl.MeshFunction("size_t", mesh, filename)

filename = "data/"+geometry+str(meshsize)+".h5"
output_file = dl.HDF5File(mesh.mpi_comm(), filename, "w")
output_file.write(mesh, "mesh")
output_file.close()

filename = "data/"+geometry+str(meshsize)+"_physical_region.h5"
output_file = dl.HDF5File(mesh.mpi_comm(), filename, "w")
output_file.write(subdomains, "subdomains")
output_file.close()

filename = "data/"+geometry+str(meshsize)+"_facet_region.h5"
output_file = dl.HDF5File(mesh.mpi_comm(), filename, "w")
output_file.write(boundaries, "facets")
output_file.close()
