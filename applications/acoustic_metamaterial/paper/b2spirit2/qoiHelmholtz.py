from __future__ import absolute_import, division, print_function

import dolfin as dl
dl.dx = dl.dx(metadata={'quadrature_degree':2, "representation":'uflacs'})

STATE = 0

class QoIHelmholtz:

    """
    misfit term of simulaation-observation at a few locations
    Q = ||O(u) - \bar{u}||^2
    O is the observation functional
    u is the state variable
    \bar{u} is the target
    """
    def __init__(self, mesh, Vh, domain, u_obs):
        """
        Constructor.
        INPUTS:
        - mesh: the mesh
        - Vh: the finite element space for the state variable
        """
        self.mesh = mesh
        self.Vh = Vh
        self.u_obs = u_obs
        v = dl.TestFunction(Vh)
        vr, vi = dl.split(v)
        self.domain = domain
        self.obs = dl.assemble(vr*domain.dx(0)) + dl.assemble(vi*domain.dx(0))
        self.state = dl.Function(Vh).vector()

    def form(self, x_fun):
        xr_fun, xi_fun = dl.split(x_fun) # x_fun.split(True)
        u_fun = self.u_obs
        ur_fun, ui_fun = u_fun.split(True)

        qoi_form = ((xr_fun-ur_fun)**2+(xi_fun-ui_fun)**2)*self.domain.dx(0)

        return qoi_form

    def eval(self, x):
        """
        Evaluate the quantity of interest at a given point in the state and
        parameter space.

        INPUTS:
        - x coefficient vector of state variable
        """

        x_fun = dl.Function(self.Vh, x)
        u_fun = self.u_obs
        xr_fun, xi_fun = x_fun.split(True)
        ur_fun, ui_fun = u_fun.split(True)

        QoI = dl.assemble(((xr_fun-ur_fun)**2+(xi_fun-ui_fun)**2)*self.domain.dx(0))

        return QoI

    def adj_rhs(self, x, rhs):
        """
        The right hand for the adjoint problem (i.e. the derivative of the Lagrangian funtional
        with respect to the state u).

        INPUTS:
        - x coefficient vector of state variable
        - rhs: FEniCS vector to store the rhs for the adjoint problem.
        """
        ### rhs = - df/dstate
        self.grad_state(x, rhs)
        rhs *= -1

    def grad_state(self, x, g):
        """
        The partial derivative of the qoi with respect to the state variable.

        INPUTS:
        - x coefficient vector of state variable
        - g: FEniCS vector to store the gradient w.r.t. the state.
        """

        x_fun = dl.Function(self.Vh, x)
        u_fun = self.u_obs
        xr_fun, xi_fun = x_fun.split(True)
        ur_fun, ui_fun = u_fun.split(True)
        x_test = dl.TestFunction(self.Vh)
        xr_test, xi_test = dl.split(x_test)

        g.zero()
        g.axpy(2., dl.assemble(((xr_fun-ur_fun)*xr_test + (xi_fun-ui_fun)*xi_test)*self.domain.dx(0)))

    def apply_ij(self,i,j, dir, out):
        """
        Apply the second variation \delta_ij (i,j = STATE,PARAMETER) of the q.o.i. in direction dir.

        INPUTS:
        - i,j integer (STATE=0, PARAMETER=1) which indicates with respect to which variables differentiate
        - dir the direction in which to apply the second variation
        - out: FEniCS vector to store the second variation in the direction dir.

        NOTE: setLinearizationPoint must be called before calling this method.
        """
        x_test = dl.TestFunction(self.Vh)
        xr_test, xi_test = dl.split(x_test)
        dir_fun = dl.Function(self.Vh, dir)
        dir_fun_r, dir_fun_i = dir_fun.split(True)
        out.zero()
        if i == STATE and j == STATE:
            out.axpy(2., dl.assemble((dir_fun_r*xr_test + dir_fun_i*xi_test)*self.domain.dx(0)))

    def apply_ijk(self, i, j, k, dir1, dir2, out):
        ## Q_xxx(dir1, dir2, x_test)
        out.zero()
        if i == STATE and j == STATE and k == STATE:
            out.axpy(0., self.obs)

    def setLinearizationPoint(self, x):
        """
        Specify the linearization point for computation of the second variations in method apply_ij.

        INPUTS:
        - x = [u,m,p] is a list of the state u, parameter m, and adjoint variable p
        """
        self.state.zero()
        self.state.axpy(1., x)