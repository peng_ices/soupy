from __future__ import absolute_import, division, print_function

plotFlag = True

if plotFlag:
    import matplotlib.pyplot as plt

import numpy as np

type_set = ['random','constant','linear','quadratic','saa']

size = 5

meshsize_set = [1, 2, 3, 4, 5]
mesh = ['dim = 2,347', 'dim = 8,795', 'dim = 34,217', 'dim = 134,796', 'dim = 535,321']

for type in type_set:

    fig = plt.figure()
    h = []
    for meshsize in meshsize_set[0:size]:
        filename = "additive"+str(meshsize)+"/data/traceMCvsSVD"+type+"PDE0.npz"
        data = np.load(filename)
        d=data["d"]

        if meshsize == 1:
            h1, = plt.semilogy(np.abs(d), 'b.-')
            h.append(h1)
        elif meshsize == 2:
            h2, = plt.semilogy(np.abs(d), 'rx-')
            h.append(h2)
        elif meshsize == 3:
            h3, = plt.semilogy(np.abs(d), 'gd-')
            h.append(h3)
        elif meshsize == 4:
            h4, = plt.semilogy(np.abs(d), 'ks-')
            h.append(h4)
        elif meshsize == 5:
            h5, = plt.semilogy(np.abs(d), 'm<-')
            h.append(h5)

    if type is 'random':
        title = "at random design"
    else:
        if type is 'constant':
            type = 'deterministic'
        title = "at optimal design with " + type +" approximation"

    plt.title(title, fontsize=12)
    plt.xlabel("$N$",fontsize=12)
    plt.ylabel("|$\lambda_N$|",fontsize=12)

    plt.legend(h[0:size], mesh[0:size], fontsize=12, loc=1)
    plt.tick_params(axis='both', which='major', labelsize=12)
    plt.tick_params(axis='both', which='minor', labelsize=12)

    filename = "figure/Eigenvalue"+type+".eps"
    fig.savefig(filename,format='eps')

if plotFlag:
    plt.show()
