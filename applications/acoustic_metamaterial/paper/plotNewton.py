
from __future__ import absolute_import, division, print_function

plotFlag = True

if plotFlag:
    import matplotlib.pyplot as plt

import numpy as np
import pickle

type_set = ['constant', 'linear', 'quadratic', 'saa']

size = 5

meshsize_set = [1, 2, 3, 4, 5]
mesh = ['dim = 4,454', 'dim = 17,114', 'dim = 67,462', 'dim = 267,640', 'dim = 1,066,761']

for type in type_set:

    fig = plt.figure()
    h = []
    for meshsize in meshsize_set[0:size]:
        filename = "additive"+str(meshsize)+"/data/"+type+"/data.p"
        data = pickle.load(open(filename, 'rb'))
        iteration = data['opt_result']['Iteration']
        costValue = data['opt_result']['costValue']
        costGrad = data['opt_result']['costGrad']

        func_ncalls = data['func_ncalls']
        grad_ncalls = data['grad_ncalls']
        hess_ncalls = data['hess_ncalls']
        cg_ncalls = data['cg_ncalls']

        costValue = costValue[:14]
        iteration = range(len(costValue))

        if meshsize == 1:
            h1, = plt.semilogy(iteration, costValue, 'b.-')
            h.append(h1)
        elif meshsize == 2:
            h2, = plt.semilogy(iteration, costValue, 'rx-')
            h.append(h2)
        elif meshsize == 3:
            h3, = plt.semilogy(iteration, costValue, 'gd-')
            h.append(h3)
        elif meshsize == 4:
            h4, = plt.semilogy(iteration, costValue, 'ks-')
            h.append(h4)
        elif meshsize == 5:
            h5, = plt.semilogy(iteration, costValue, 'm<-')
            h.append(h5)

    if type is 'random':
        title = "at random design"
    else:
        if type is 'constant':
            type = 'deterministic'
        title = "at optimal design with " + type +" approximation"
    plt.title(title, fontsize=12)
    plt.xlabel("# quasi Newton iterations",fontsize=12)
    plt.ylabel("objective functional", fontsize=12)
    plt.xlim([0, 10])
    # if type is 'constant' or type is 'linear':
    #     plt.ylim([1e-2, 1e3])
    # else:
    #     plt.ylim([1e0, 1e3])
    plt.legend(h[0:size], mesh[0:size], fontsize=12, loc=1)
    plt.tick_params(axis='both', which='major', labelsize=12)
    plt.tick_params(axis='both', which='minor', labelsize=12)

    filename = "figure/quasiNewton"+type+".eps"
    fig.savefig(filename, format='eps')

if plotFlag:
    plt.show()

