from __future__ import absolute_import, division, print_function

import math
import numpy as np
import dolfin as dl
dl.dx = dl.dx(metadata={'quadrature_degree':2, "representation":'uflacs'})
dl.set_log_active(False)

# import mshr as mh
import pickle
from qoiDiffusion import QoIDiffusion

# path = ""
path = "../../"

import sys
sys.path.append(path)
from soupy import *

# # samples used to compute mean square error
N_mse = 10

# choose mesh size, [1, 2, 3, 4, 5]
meshsize = 2

optMethod = 'home_bfgs'
# optMethod = 'home_ncg'
# optMethod = 'scipy_bfgs'
# optMethod = 'fmin_ncg'

if optMethod is 'home_bfgs':
    bfgs_ParameterList = BFGS_ParameterList()
    bfgs_ParameterList["max_iter"] = 32
    bfgs_ParameterList["LS"]["max_backtracking_iter"] = 20
elif optMethod is 'home_ncg':
    ncg_ParameterList = ReducedSpaceNewtonCG_ParameterList()
    ncg_ParameterList["globalization"] = "LS"
    ncg_ParameterList["max_iter"] = 32
    ncg_ParameterList["LS"]["max_backtracking_iter"] = 20
elif optMethod is 'scipy_bfgs':
    from scipy.optimize import fmin_l_bfgs_b

plotFlag = False

############ 0 parameters to config ###############

## use Monte Carlo correction True/False
correction = False

# run the constant approximation True/False
constant_run = True
# run the linear approximation True/False
linear_run = False
# run the quadratic approximation True/False
quadratic_run = False
# run the SAA approximation True/False
saa_run = False

# check gradient True/False
check_gradient = False
# check Hessian True/False
check_hessian = False

if correction:
    parameter = pickle.load(open('data/parameter.p', 'rb'))
    parameter["correction"] = correction
else:
    parameter = dict()
    # with Monte Carlo correction or not
    parameter["correction"] = correction
    # optimization method
    parameter["optMethod"] = optMethod
    # mesh size
    parameter["meshsize"] = meshsize
    parameter["optType"] = 'vector'
    parameter["optDimension"] = 16
    parameter["bounds"] = [(0., 32.) for i in range(parameter["optDimension"])]
    # number of samples for trace estimator
    parameter['N_tr'] = 50
    # number of samples for Monte Carlo correction
    parameter['N_mc'] = 10
    # regularization coefficient
    parameter['alpha'] = 1.e-8  # alpha*R(z)
    # variance coefficient
    parameter['beta'] = 0.0     # E[Q] + beta Var[Q]
    # prior covariance, correlation length prop to gamma/delta
    parameter['delta'] = 20.    # delta*Identity
    parameter['gamma'] = 0.1    # gamma*Laplace
    parameter["dim"] = 1

    pickle.dump(parameter, open('data/parameter.p','wb'))

################### 1. Define the Geometry ###########################

dl.parameters["ghost_mode"] = "shared_facet"

# 1. Define the Geometry
dl.set_log_active(False)
sep = "\n" + "#" * 80 + "\n"
nx = 1024
mesh = dl.IntervalMesh(dl.mpi_comm_self(), nx, -1., 1.)


comm = mesh.mpi_comm()
mpi_comm = mesh.mpi_comm()
if hasattr(mpi_comm, "rank"):
    mpi_rank = mpi_comm.rank
    mpi_size = mpi_comm.size
else:
    mpi_rank = 0
    mpi_size = 1

#################### 2. Define optimization PDE problem #######################

Vh_STATE = dl.FunctionSpace(mesh, 'CG', 1)
Vh_PARAMETER = dl.FunctionSpace(mesh, "CG", 1)
Vh_OPTIMIZATION = dl.FunctionSpace(mesh, "CG", 1)
Vh = [Vh_STATE, Vh_PARAMETER, Vh_STATE, Vh_OPTIMIZATION]

# Define Dirichlet boundary (x = -1 or x = 1)
def boundary(x):
    return x[0] < -1.0 + dl.DOLFIN_EPS or x[0] > 1.0 - dl.DOLFIN_EPS

u_bdr = dl.Constant(0.) # dl.Expression("0.0", degree=1)
u_bdr0 = dl.Constant(0.) # dl.Expression("0.0", degree=1)
bcs = dl.DirichletBC(Vh[STATE], u_bdr, boundary)
bcs0 = dl.DirichletBC(Vh[STATE], u_bdr0, boundary)


def residual(u, m, p, z):

    form = dl.exp(m)*dl.inner(dl.nabla_grad(u), dl.nabla_grad(p))*dl.dx - z*p*dl.dx

    return form


pde = ControlPDEProblem(Vh, residual, bcs, bcs0, is_fwd_linear=True)

u_d = dl.Function(Vh_STATE).vector()
x_all = [dl.Function(Vh[i]).vector() for i in range(4)]
z_e = dl.Expression("pi*pi*sin(pi*x[0])", degree=2)
z = dl.interpolate(z_e, Vh_OPTIMIZATION)
x_all[OPTIMIZATION].set_local(z.vector().get_local()) #2*np.ones(u_d.size())
pde.solveFwd(u_d, x_all)
u_d = vector2Function(u_d, Vh_STATE)

################## 3. Define the quantity of interest (QoI) ############

qoi = QoIDiffusion(mesh, Vh, u_d)

################## 4. Define Penalization term ############################

penalization = L2Penalization(Vh[OPTIMIZATION], dl.dx, parameter["alpha"])

################## 5. Define the prior ##################### ##############
# anis_diff = dl.Expression(code_AnisTensor2D, degree=2)
# anis_diff.theta0 = 1.
# anis_diff.theta1 = 1.
# anis_diff.alpha = 0.*np.pi/4
#
# if correction:
#     ############ begin load atrue  #####################
#     atrue_array = pickle.load( open( "data/atrue_array.p", "rb" ) )
#     atrue_fun = dl.Function(Vh_PARAMETER, name="sample")
#     atrue = atrue_fun.vector()
#     atrue[:] = atrue_array[:]
#     ############ finish load atrue #####################
# else:
#     ########## begin create atrue ####################
#     gamma_atrue = .01
#     delta_atrue = 4
#
#     def true_model(Vh, gamma, delta, anis_diff):
#         prior = BiLaplacianPrior(Vh, gamma, delta, anis_diff)
#         noise = dl.Vector()
#         prior.init_vector(noise,"noise")
#         noise_size = noise.get_local().shape[0]
#         noise_true = np.random.randn(noise_size)
#         np.save("data/noise_true",noise_true)
#         noise_true = np.load("data/noise_true.npy")
#         noise.set_local( noise_true)
#         # noise = dl.Vector()
#         # prior.init_vector(noise,"noise")
#         # Random.normal(noise, 1., True)
#         atrue = dl.Vector()
#         prior.init_vector(atrue, 0)
#         prior.sample(noise,atrue)
#         return atrue
#
#     atrue = true_model(Vh[PARAMETER], gamma_atrue, delta_atrue, anis_diff)
#     atrue_fun = vector2Function(atrue, Vh_PARAMETER, name="sample")
#     # dl.plot(atrue_fun)
#
#     locations = np.array([[0.2, 0.1], [0.2, 0.9], [1,.5], [1.8, .1], [1.8, .9]])
#     pen = 2e1
#     gamma_moll = 1.
#     delta_moll = 1.
#     prior = MollifiedBiLaplacianPrior(Vh[PARAMETER], gamma_moll, delta_moll, locations, atrue, anis_diff, pen)
#     atrue = prior.mean
#     atrue_fun = vector2Function(atrue, Vh_PARAMETER, name="sample")
#     # dl.plot(atrue_fun)
#
#     atrue_array = np.array(atrue.array())
#     pickle.dump( atrue_array, open( "data/atrue_array.p", "wb" ) )
#     dl.File("data/atrue.pvd") << atrue_fun
#     ########### finish create atrue ####################

prior = BiLaplacianPrior(Vh[PARAMETER], parameter["gamma"], parameter["delta"])
