from __future__ import absolute_import, division, print_function

import dolfin as dl
dl.dx = dl.dx(metadata={'quadrature_degree':2, "representation":'uflacs'})
import numpy as np

path = "../../"

import sys
sys.path.append(path)
from soupy.utils.variables import STATE, PARAMETER, ADJOINT
from soupy.utils.vector2function import vector2Function


class QoIDiffusion:

    """
    misfit term of simulaation-observation at a few locations
    Q = ||O(u) - \bar{u}||^2
    O is the observation functional
    u is the state variable
    \bar{u} is the target
    """
    def __init__(self, mesh, Vh, u_d):
        """
        Constructor.
        INPUTS:
        - mesh: the mesh
        - Vh_STATE: the finite element space for the state variable
        """
        # self.u_d = dl.Expression("-x[0]*x[0]+1", degree=1)
        self.u_d = u_d
        self.x = dl.Function(Vh[STATE]).vector()
        self.Vh = Vh
        # u = dl.TrialFunction(Vh[STATE])
        # v = dl.TestFunction(Vh[STATE])
        # self.Mass = dl.assemble(u*v*dl.dx)
        # self.udiff = dl.Function[Vh[STATE]].vector()
        # self.uhelp = dl.Function(Vh[STATE]).vector()
        self.u_test = dl.TestFunction(Vh[STATE])

    def form(self, x_fun):
        """
        Build the weak form of the qoi
        :param x:
        :return:
        """

        return (x_fun-self.u_d)*(x_fun-self.u_d)*dl.dx

    def eval(self, x):
        """
        Evaluate the quantity of interest at a given point in the state and
        parameter space.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        """
        x_fun = vector2Function(x, self.Vh[STATE])
        # print("x", x, x[STATE])
        # x_fun = [None] * len(x)
        # for i in range(len(x)):
        #     x_fun[i] = vector2Function(x[i], self.Vh[i])

        # x_fun = [vector2Function(x[i], self.Vh[i]) for i in range(4)]
        # self.udiff.zero()
        # self.udiff.axpy(1.0, self.u_d.vector())
        # self.udiff.axpy(-1.0, x[STATE])
        # QoI = self.udiff.inner(self.Mass.mult(self.udiff, self.udiff))
        # self.Mass.mult(self.udiff, self.uhelp)
        # QoI = self.udiff.inner(self.uhelp)

        # QoI = dl.assemble((x_fun[STATE]-self.u_d)*(x_fun[STATE]-self.u_d)*dl.dx)

        QoI = dl.assemble(self.form(x_fun))

        return QoI

    def adj_rhs(self,x,rhs):
        """
        The right hand for the adjoint problem (i.e. the derivative of the Lagrangian funtional
        with respect to the state u).

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - rhs: FEniCS vector to store the rhs for the adjoint problem.
        """
        ### rhs = - df/dstate
        self.grad_state(x, rhs)
        rhs *= -1

    def grad_state(self, x, g):
        """
        The partial derivative of the qoi with respect to the state variable.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - g: FEniCS vector to store the gradient w.r.t. the state.
        """
        # x_fun = [None] * len(x)
        # for i in range(len(x)):
        #     x_fun[i] = vector2Function(x[i], self.Vh[i])
        x_fun = vector2Function(x, self.Vh[STATE])
        g.zero()
        g.axpy(1.0, dl.assemble(2*(x_fun-self.u_d)*self.u_test*dl.dx))

    def grad_parameter(self, x, g):
        """
        The partial derivative of the qoi with respect to the parameter variable.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - g: FEniCS vector to store the gradient w.r.t. the parameter.
        """
        g.zero()

    def grad_optimization(self, x, g):
        """
        The partial derivative of the qoi with respect to the optimization variable.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        - g: FEniCS vector to store the gradient w.r.t. the optimization.
        """
        g.zero()

    def apply_ij(self,i,j, dir, out):
        """
        Apply the second variation \delta_ij (i,j = STATE,PARAMETER,OPTIMIZATION) of the q.o.i. in direction dir.

        INPUTS:
        - i,j integer (STATE=0, PARAMETER=1, OPTIMIZATION=3) which indicates with respect to which variables differentiate
        - dir the direction in which to apply the second variation
        - out: FEniCS vector to store the second variation in the direction dir.

        NOTE: setLinearizationPoint must be called before calling this method.
        """
        out.zero()
        # if i == STATE and j == STATE:
        #     self.Mass.mult(dir, self.uhelp)
        #     out.axpy(2.0, self.uhelp)

    def apply_ijk(self,i,j,k,dir1,dir2, out):
        """
        Apply the third variation \delta_ijk (i,j,k = STATE,PARAMETER,OPTIMIZATION) of the q.o.i. in direction dir1, dir2

        INPUTS:
        - i,j,k integer (STATE=0, PARAMETER=1, OPTIMIZATION=3) which indicates with respect to which variables differentiate
        - dir1,dir2 the direction in which to apply the second and third variations
        - out: FEniCS vector to store the second variation in the direction dir.

        NOTE: setLinearizationPoint must be called before calling this method.
        """
        out.zero()

    def setLinearizationPoint(self, x):
        """
        Specify the linearization point for computation of the variations in method apply_ij and apply_ijk.

        INPUTS:
        - x coefficient vector of [state,parameter,adjoint,optimization] variable
        """
        self.x.zero()
        self.x.axpy(1., x)
        # for i in range(len(x)):
        #     self.x[i].zero()
        #     self.x[i].axpy(1., x[i])
